#!/usr/bin/env python
# coding: utf-8
import sys
import calc

""" Example
>> a # b := a * a + b * b
>> a @ b := (a # a) + (b # b)
2 @ 3 = 2 # 2 + 3 # 3 = (2 * 2 + 2 * 2) + (3 * 3 + 3 * 3) = 8 + 18 = 26
>> p U q := (p V q) % (p + q)
>> p V q := (p * p) % (q * q)
3 U 2 = (3 V 2) % (3 + 2) = ((3 * 3) % (2 * 2)) % (3 + 2) = (9 % 4) % 5 = 1 % 5 = 1
"""

if __name__ == '__main__':
    print("***** tacalc v0.1 *****")
    x = [0.0 for i in range(5)]
    new_x = [0.0 for i in range(5)]
    lambdas = calc.defope.Lambda()
    while True:
        sys.stdout.write(">> ")
        f = sys.stdin.readline().strip()
        # パース
        parser = calc.parse.Parser(f, x, lambdas)
        try:
            ast = parser.parse()
            if not isinstance(ast, calc.expr.Expr):
                # 定義式
                print("New definition: {1}{0}{2} = {3}".format(ast[0], ast[1], ast[2], ast[3]))
                continue
        except calc.token.TokenizerException, e:
            print("[ERROR] TokenizerException")
            print(e)
            continue
        except calc.parse.ParserException, e:
            print("[ERROR] ParserException")
            print(e)
            continue
        except Exception, e:
            print("[FATAL] Exception")
            print(e)
            continue
        # 評価
        try:
            new_x.pop(4)
            value = ast.evaluate()
            if isinstance(value, float):
                new_x.reverse()
                new_x.append(value)
                new_x.reverse()
                print(" = {0}".format(value))
            else:
                print("def: {1} {0} {2} = {3}".format(value[0], value[1], value[2], value[3]))
            for i in range(5):
                print("x{0}: {1}".format(i + 1, new_x[i]))
        except Exception, e:
            print("[FATAL] Exception")
            print(e)
            continue
        # 更新
        x = list(new_x)
