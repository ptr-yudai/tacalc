# coding: utf-8

""" 字句解析器 """
class Tokenizer(object):
    def __init__(self, source, lambdas):
        """ 初期化 """
        self.source = source
        self.index = 0
        self.token = None
        self.lambdas = lambdas

    def empty(self):
        """ 全て読み取ったか """
        return len(self.source) - 1 < self.index

    def dump(self):
        """ 残りをすべて吐き出す """
        output = ""
        while not self.empty():
            output += self.next_token()
        return output

    def next_char(self):
        """ 次の文字を取り出す """
        if self.empty():
            return None
        c = self.source[self.index]
        self.index += 1
        return c

    def peek_char(self):
        """ 次の文字を先読みする """
        if self.empty():
            return None
        return self.source[self.index]

    def next_token(self):
        """ 次の字句を取り出す """
        self.token = self.next_char()
        if self.token is None:
            return None
        # 空白を飛ばす
        while self.token == ' ':
            self.token = self.next_char()
        if self.is_digit(self.token):
            # 数字
            if not self.empty():
                point = False
                c = self.next_char()
                while self.is_digit(c):
                    if c == '.':
                        # 小数点
                        if point:
                            raise TokenizerException(
                                "Invalid token near '{0}' at {1}".format(
                                    self.token + c, self.index
                                )
                            )
                        else:
                            point = True
                    self.token += c
                    if self.empty():
                        break
                    c = self.next_char()
                if not self.empty() and self.is_unknown(c):#not self.is_operator(c) and not self.lambdas.is_defined(c):
                    raise TokenizerException(
                        "Invalid token near '{0}' at {1}".format(
                            self.token + c, self.index
                        )
                    )
                self.index -= 1
        elif self.token == 'x':
            # レジスタ
            if self.peek_char() in ['1', '2', '3', '4', '5']:
                self.token += self.next_token()
            else:
                raise TokenizerException(
                    "Invalid token near '{0}' at {1}".format(
                        self.token + self.peek_char(), self.index
                    )
                )
        elif self.is_operator(self.token):
            # 演算子
            pass
        elif self.token in '()':
            # 括弧
            pass
        elif self.token == ':':
            # 定義記号
            if self.peek_char() == '=':
                self.token += self.next_token()
        else:
            # 不明なシンボル
            while not self.empty() and self.is_unknown(self.peek_char()):
                self.token += self.next_char()
        return self.token

    def peek_token(self):
        """ 次の字句を先読みする """
        index = self.index
        token = self.next_token()
        if token is not None:
            self.index = index
        return token
            
    def is_digit(self, c):
        """ 数字かどうか """
        return c in '0123456789.'

    def is_value(self, t):
        """ 値かどうか """
        dot = False
        for c in t:
            if c == '.':
                if dot:
                    return False
                else:
                    dot = True
            elif not self.is_digit(c):
                return False
        return True

    def is_variable(self, c):
        """ 変数かどうか """
        if len(c) < 2:
            return False
        if c[0] == 'x':
            if c[1] in ['1', '2', '3', '4', '5']:
                return True
        return False
        
    def is_operator(self, c):
        """ 演算子かどうか """
        return c in ['+', '-', '/', '*', '%', ' ']

    def is_unknown(self, t):
        """ 未定義かどうか """
        if self.is_operator(t):
            return False
        if self.is_value(t):
            return False
        if self.is_variable(t):
            return False
        if self.lambdas.is_defined(t):
            return False
        if t == ' ' * len(t):
            return False
        if t in "()":
            return False
        return True

class TokenizerException(Exception):
    pass
