# coding: utf-8
from token import Tokenizer
from expr_val import ExprValue
from expr_op1 import ExprOp1
from expr_op2 import ExprOp2
from reg import Register
from ope import *

""" パーサ """
class Parser(object):
    """
    X ? Y := G(X, Y)

    G  -> PG
    G' -> %PG' | e
    P  -> EP
    P' -> <ope>EP' | e
    E  -> TE
    E' -> +TE' | -TE' | e
    T  -> FT
    T' -> *FT' | /FT' | //FT' | e
    F  -> F'
    F' -> (C) | +N | -N | N
    N  -> 0123456789 | x1 | x2 | x3 | x4 | x5
    """
    def __init__(self, expr, reg, lambdas):
        """ 初期化 """
        self.tokenizer = Tokenizer(expr, lambdas)
        self.reg = reg

    def parse(self):
        """ パース """
        this_token = self.tokenizer.next_token()
        next_token = self.tokenizer.peek_token()
        self.tokenizer.token = this_token
        if self.tokenizer.is_unknown(next_token):
            # 定義式
            return self.universe()
        else:
            # 計算式
            return self.galaxy1()

    def universe(self):
        l = self.tokenizer.token
        ope = self.tokenizer.next_token()
        r = self.tokenizer.next_token()
        if self.tokenizer.next_token() != ":=":
            raise ParserException("Expected ':=' but got '{0}'".format(self.tokenizer.token))
        form = self.tokenizer.dump()
        self.tokenizer.lambdas.add_operator(ope, l, r, form)
        return ope, l, r, form

    def galaxy1(self):
        """ G -> EG """
        expr = self.planet1()
        return self.galaxy2(expr)

    def galaxy2(self, expr1):
        """ G -> %EG' | e """
        if self.tokenizer.token == '%':
            self.tokenizer.next_token()
            expr2 = self.planet1()
            return self.galaxy2(ExprOp2(op_mod, expr1, expr2))
        else:
            return expr1
    
    def planet1(self):
        """ P  -> EP """
        expr = self.expr1()
        return self.planet2(expr)
        
    def planet2(self, expr1):
        """ P' -> <ope>EP' | e """
        if self.tokenizer.lambdas.is_defined(self.tokenizer.token):
            ope = self.tokenizer.token
            self.tokenizer.next_token()
            expr2 = self.planet1()
            # 定義演算は新しく構文解析
            ope, l, r, form = self.tokenizer.lambdas.get_operator(ope)
            if len(l) > len(r):
                form = form.replace(l, str(expr1.evaluate()))
                form = form.replace(r, str(expr2.evaluate()))
            else:
                form = form.replace(r, str(expr2.evaluate()))
                form = form.replace(l, str(expr1.evaluate()))
            parser = Parser(form, self.reg, self.tokenizer.lambdas)
            ast = parser.parse()
            value = ast.evaluate()
            return ExprValue(value)
        else:
            return expr1

    def expr1(self):
        """ E  -> TE """
        expr = self.term1()
        return self.expr2(expr)

    def expr2(self, expr1):
        """ E' -> +TE' | -TE' | e """
        if self.tokenizer.token == '+':
            self.tokenizer.next_token()
            expr2 = self.term1()
            return self.expr2(ExprOp2(op_add, expr1, expr2))
        elif self.tokenizer.token == '-':
            self.tokenizer.next_token()
            expr2 = self.term1()
            return self.expr2(ExprOp2(op_sub, expr1, expr2))
        else:
            return expr1

    def term1(self):
        """ T  -> FT """
        expr = self.factor1()
        return self.term2(expr)

    def term2(self, expr1):
        """ T' -> *FT' | /FT' | //FT' | e """
        if self.tokenizer.token == '*':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_mul, expr1, expr2))
        elif self.tokenizer.token == '/':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_div, expr1, expr2))
        elif self.tokenizer.token == '//':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_ddiv, expr1, expr2))
        else:
            return expr1

    def factor1(self):
        """ F  -> F' """
        expr1 = self.factor2()
        return expr1

    def factor2(self):
        """ F' -> (C) | +N | -N | N """
        if self.tokenizer.is_digit(self.tokenizer.token[0]):
            return self.value()
        elif self.tokenizer.is_variable(self.tokenizer.token):
            return self.variable()
        elif self.tokenizer.token == '+':
            self.tokenizer.next_token()
            return self.value()
        elif self.tokenizer.token == '-':
            self.tokenizer.next_token()
            return ExprOp1(op_neg, self.value())
        elif self.tokenizer.token == '(':
            self.tokenizer.next_token()
            expr = self.galaxy1()
            self.tokenizer.next_token()
            return expr
        else:
            raise ParserException("Invalid expression.")

    def value(self):
        """ N -> Value """
        expr = ExprValue(float(self.tokenizer.token))
        self.tokenizer.next_token()
        return expr

    def variable(self):
        """ N -> Variable """
        expr = Register(self.reg, self.tokenizer.token)
        self.tokenizer.next_token()
        return expr

class ParserException(Exception):
    pass
