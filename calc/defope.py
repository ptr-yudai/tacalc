# coding: utf-8

""" 定義演算子リスト """
class Lambda(object):
    def __init__(self):
        self.definition = []

    def add_operator(self, ope, l, r, form):
        """ 演算子を定義する """
        if len(ope) != 1:
            raise LambdaException("Operator must be a character.")
        self.definition.append(
            (ope, l, r, form)
        )

    def get_operator(self, ope):
        for l in self.definition:
            if l[0] == ope:
                return l
        return None

    def is_defined(self, ope):
        for l in self.definition:
            if l[0] == ope:
                return True
        return False

class LambdaException(Exception):
    pass
