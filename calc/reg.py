# coding: utf-8
from expr import Expr

""" レジスタ """
class Register(Expr):
    def __init__(self, reg, index):
        """ 初期化 """
        self.reg = reg
        self.index = int(index[1]) - 1

    def evaluate(self):
        """ 評価 """
        return self.reg[self.index]
